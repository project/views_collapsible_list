INTRODUCTION
------------

The Views Collapsible List module provides a new style plugin for Views that
renders items in a list where you can expand and collapse additional information
for an item. You can select multiple fields to be shown/hidden by this toggle.
This allows you to present additional information to users if they want to see
it without having too much clutter when the page initially loads.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/views_collapsible_list

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/views_collapsible_list


REQUIREMENTS
------------

This module requires the following modules:

 * Views (https://drupal.org/project/views)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


CONFIGURATION
-------------

 * Apply the collapsible list style to your View:

   - Under "Style", select the "Collapsible List" option.
   - Add some fields to the View definition.
   - Open the style settings and choose which of the fields you want to be
     collapsible.


MAINTAINERS
-----------

Current maintainers:
 * Kevin Dutra (kevin.dutra) - https://www.drupal.org/user/1312744

This project has been sponsored by:
 * Workday
   Workday is a leading provider of enterprise cloud applications for finance
   and human resources. Visit http://www.workday.com for more information.
